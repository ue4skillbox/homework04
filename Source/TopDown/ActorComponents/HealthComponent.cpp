// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDown/ActorComponents/HealthComponent.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent() {
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these
    // features off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UHealthComponent::BeginPlay() {
    Super::BeginPlay();

    CurrentHealth = MaxHealth;
}

void UHealthComponent::ApplyDamage(float Damage) {
    float ResultHealth = CurrentHealth - Damage;
    if (ResultHealth > 0) {
        CurrentHealth = ResultHealth;
        OnHealthChange.Broadcast(CurrentHealth, Damage);
    } else {
        OnHealthChange.Broadcast(0.f, CurrentHealth);
        CurrentHealth = 0.f;
        OnDead.Broadcast();
    }
}
