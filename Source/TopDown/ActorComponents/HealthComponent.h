// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"

#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWN_API UHealthComponent : public UActorComponent {
    GENERATED_BODY()

protected:
    float MaxHealth = 100.f;
    float CurrentHealth = MaxHealth;

public:
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
    FOnHealthChange OnHealthChange;
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
    FOnDead OnDead;

public:
    // Sets default values for this component's properties
    UHealthComponent();

    void ApplyDamage(float Damage);

protected:
    // Called when the game starts
    virtual void BeginPlay() override;
};
