// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "TopDown/ActorComponents/HealthComponent.h"

#include "CharacterHealthComponent.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWN_API UCharacterHealthComponent : public UHealthComponent {
    GENERATED_BODY()
};
